cat <<REPOSITORIOS> /etc/apk/repositories
http://dl-cdn.alpinelinux.org/alpine/v3.14/main
http://dl-cdn.alpinelinux.org/alpine/v3.14/community
@edge http://dl-cdn.alpinelinux.org/alpine/edge/main
@edge http://dl-cdn.alpinelinux.org/alpine/edge/community
@edge http://dl-cdn.alpinelinux.org/alpine/edge/testing
REPOSITORIOS


# Adicionar usuráio não root
adduser -D administrador
echo "administrador:12345" | chpasswd


# Identificar a interface  de rede ativa
interface=$(iproute  |  grep default | cut -d" " -f 5)

cat <<REDE> /etc/network/interfaces
auto lo
iface lo inet loopback

auto br0
iface br0 inet dhcp
	bridge-ports  $interface
	bridge-stp 0
REDE

#SCRIPT DE BRIDGER

cat <<REDE> rede.sh
 apk add bridge
 brctl addbr br0
 brctl addif br0 $interface
 ip link set dev $interface up
 rc-service networking restart
 rc-service lxcfs start &
 rc-service lxd start
#reboot 
REDE

chmod +x rede.sh

# instalar LXC
#apk add lxd@edge lxcfs dbus
apk add lxd@edge lxcfs dbus lxd-client@edge


# Parametrizar o LXC/LXD
echo "session optional pam_cgfs.so -c freezer,memory,name=systemd,unified" >> /etc/pam.d/system-login
echo "lxc.idmap = u 0 100000 65536" >> /etc/lxc/default.conf
echo "lxc.idmap = g 0 100000 65536" >> /etc/lxc/default.conf
echo "root:100000:65536" >> /etc/subuid
echo "root:100000:65536" >> /etc/subgid
sed -i "s/#systemd_container\=no/systemd_container=yes/g" /etc/conf.d/lxc


# Adicionar o LXC/LXD no serviços de inicialização
rc-update add lxc
rc-update add lxd
rc-update add lxcfs
rc-update add dbus

# Configurar a interface bridge e reinicar o sistema.
./rede.sh
lxd init
